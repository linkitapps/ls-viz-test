import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import pkg from './package.json';

export default [
	// browser-friendly UMD build
	{
		entry: 'src/core/full.js',
		dest: pkg.fullLib,
		format: 'umd',
		moduleName: 'lsViz',
		plugins: [
			resolve(), // so Rollup can find `ms`
			commonjs() // so Rollup can convert `ms` to an ES module
		]
	},

    {
        entry: 'src/core/highcharts.js',
        dest: pkg.highchartsLib,
        format: 'umd',
        moduleName: 'lsViz',
        plugins: [
            resolve(), // so Rollup can find `ms`
            commonjs() // so Rollup can convert `ms` to an ES module
        ]
    },

    {
        entry: 'src/core/vis.js',
        dest: pkg.visLib,
        format: 'umd',
        moduleName: 'lsViz',
        plugins: [
            resolve(), // so Rollup can find `ms`
            commonjs() // so Rollup can convert `ms` to an ES module
        ]
    }
];