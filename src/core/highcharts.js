import Highcharts from 'highcharts';
import printMessage from '../printMessage.js';

export default {
    // 차트 생성
    chart: function(options, detailOptions) {
        switch (options.type) {
            // type 이 Highcharts 인 경우
            case 'line':
            case 'bar':
            case 'pie':

                // Highchart 전용 옵션인 detailOptions 를 공통 옵션인 options 와 합침
                let chartOptions = Highcharts.merge(detailOptions, {
                    chart: {
                        type: options.type
                    }
                });
                // Highcharts 생성
                Highcharts.chart(options.container, chartOptions);

                break;
        }
    },
    // utility
    util: {
        // 직접 개발 module : printMessage 는 직접 개발 module 을 포함할 수 있다는 테스트로 만듬
        printMessage: function(message) {
            return printMessage(message);
        }
    }
};