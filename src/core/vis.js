import vis from 'vis';
import printMessage from '../printMessage.js';

export default {
    // 차트 생성
    chart: function(options, detailOptions) {
        switch (options.type) {
            // type 이 vis.js 인 경우
            case 'graph3d':

                return new vis.Graph3d(options.container, detailOptions.data, detailOptions);

                break;
        }
    },
    // utility
    util: {
        // vis.js
        visDataSet: function() {
            return new vis.DataSet();
        },

        // 직접 개발 module : printMessage 는 직접 개발 module 을 포함할 수 있다는 테스트로 만듬
        printMessage: function(message) {
            return printMessage(message);
        }
    }
};