export default function printMessage (message) {
    let content = null;

	if (message) {
        content = 'Message is ' + message + '.';
    } else {
        content = 'No message.';
    }

	return message;
}